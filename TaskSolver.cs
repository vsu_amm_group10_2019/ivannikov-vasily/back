using System.Collections.Generic;

namespace Ivannikov
{
    public partial class TaskSolver
    {
        private readonly Point[] Distances = {
            new Point(1, 0),
            new Point(0, 1),
            new Point(-1, 0),
            new Point(0, -1)
        };
        private readonly int[,] _board;

        public TaskSolver(int[,] board)
        {
            _board = board;
        }

        private bool IsValidCoord(Point point)
            => point.X >= 0 && point.X < _board.GetLength(1)
                && point.Y >= 0 && point.Y < _board.GetLength(0);

        public IEnumerable<Point> FindMinPath(Point startPoint, Point endPoint)
        {
            if (!IsValidCoord(startPoint) || !IsValidCoord(endPoint)
                || _board[startPoint.Y, startPoint.X] == 0
                || _board[startPoint.Y, startPoint.X] == _board[endPoint.Y, endPoint.X])
                return null;

            var visited = new (int, int)[_board.GetLength(0), _board.GetLength(0)];
            for (int i = 0; i < visited.GetLength(0); ++i)
                for (int j = 0; j < visited.GetLength(1); ++j)
                    visited[i, j] = (-1, -1);

            visited[startPoint.Y, startPoint.X] = (-2, 0);
            var queue = new Queue<Node>();
            queue.Enqueue(new Node(startPoint, 0));
            bool found = false;

            bool move(Point pointCurrent, Point pointNext, int deep)
            {
                if (!IsValidCoord(pointNext))
                    return false;

                if (visited[pointNext.Y, pointNext.X].Item1 != -1)
                {
                    if (deep < visited[pointNext.Y, pointNext.X].Item2) return true;
                    return false;
                }

                if (_board[pointNext.Y, pointNext.X] == _board[startPoint.Y, startPoint.X])
                    return false;

                visited[pointNext.Y, pointNext.X] = (pointCurrent.Y * _board.GetLength(1) + pointCurrent.X, deep + 1);
                queue.Enqueue(new Node(pointNext, deep + 1));

                if (_board[pointNext.Y, pointNext.X] != 0)
                    return false;

                return true;
            }

            while (queue.Count > 0)
            {
                Node node = queue.Dequeue();

                if (node.Point.Equals(endPoint))
                {
                    found = true;
                    break;
                }

                for (int i = 0; i < 4; ++i)
                {
                    var j = 1;
                    while (move(node.Point, new Point(node.Point.X + Distances[i].X * j, node.Point.Y + Distances[i].Y * j), node.Deep))
                        ++j;
                }
            }

            var path = new List<Point>();
            if (found)
            {
                path.Add(endPoint);
                var i = (endPoint.Y, endPoint.X);
                while (visited[i.Y, i.X].Item1 != -2)
                {
                    var cPrev = new Point(visited[i.Y, i.X].Item1 % _board.GetLength(1), visited[i.Y, i.X].Item1 / _board.GetLength(1));
                    path.Add(cPrev);
                    i = (cPrev.Y, cPrev.X);
                }
                path.Reverse();
            }

            return path;
        }
    }
}
