namespace Ivannikov
{
    public partial class TaskSolver
    {
        private readonly struct Node
        {
            public Point Point { get; }
            public int Deep { get; }

            public Node(Point point, int deep)
            {
                Point = point;
                Deep = deep;
            }
        };
    }
}
