using System;

namespace Ivannikov
{
    public readonly struct Point : IEquatable<Point>
    {
        public int X { get; }
        public int Y { get; }

        public Point(int x, int y)
        {
            X = x;
            Y = y;
        }

        public static Point operator +(Point first, Point second)
            => new(first.X + second.X, first.Y + second.Y);

        public override string ToString()
            => $"{X} {Y}";

        public static bool TryParse(string str, out Point point)
        {
            point = default;
            string[] splited = str.Split(' ', StringSplitOptions.RemoveEmptyEntries | StringSplitOptions.TrimEntries);
            
            if (splited.Length != 2)
                return false;

            if (int.TryParse(splited[0], out var x) && int.TryParse(splited[1], out var y))
            {
                point = new Point(x, y);
                return true;
            }
            
            return false;
        }

        public bool Equals(Point other)
            => X == other.X && Y == other.Y;

        public override bool Equals(object obj)
            => obj is Point point && Equals(point);

        public override int GetHashCode()
            => (X, Y).GetHashCode();

        public static bool operator ==(Point left, Point right)
            => left.Equals(right);

        public static bool operator !=(Point left, Point right)
            => !(left == right);
    }
}
