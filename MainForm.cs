﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace Ivannikov
{
    public partial class MainForm : Form
    {
        private const int FriendFigureIndex = -1;
        private const int EnemyFigureIndex = -2;
        private const int BoardWidth = 8;
        private const int BoardHeight = 8;
        private readonly FiguresInputValidator _validator;

        public MainForm()
        {
            InitializeComponent();
            _validator = new FiguresInputValidator(txtWhiteFigures, txtBlackFigures, txtStartRookCoords, txtStopRookCoords, radWhiteRookColor, BoardWidth, BoardHeight);
            
            var whitesFigures = new (int, int)[] { (1, 0), (6, 2), (5, 3), (2, 4), (7, 4), (5, 6), (2, 7) };
            var blackFigures = new (int, int)[] { (5, 0), (3, 1), (1, 3), (4, 4), (1, 5), (6, 6), (0, 7) };

            txtWhiteFigures.Text = string.Join(Environment.NewLine, whitesFigures.Select(x => $"{x.Item1} {x.Item2}"));
            txtBlackFigures.Text = string.Join(Environment.NewLine, blackFigures.Select(x => $"{x.Item1} {x.Item2}"));
        }

        private void BtnSolvedClicked(object sender, EventArgs e)
        {
            try
            {
                int[,] board = _validator.GetBoard(EnemyFigureIndex, FriendFigureIndex);
                Point startPoint = _validator.ParseCoordsLine(txtStartRookCoords.Text);
                Point stopPoint = _validator.ParseCoordsLine(txtStopRookCoords.Text);
                
                var solver = new TaskSolver(board);
                IEnumerable<Point> solve = solver.FindMinPath(startPoint, stopPoint);
                if (solve != null && solve.Any())
                {
                    txtAnswers.Text = string.Join(Environment.NewLine, solve);
                }
                else
                {
                    MessageBox.Show(this, "Не удалось найти решение.", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            } catch (ValidateException ex)
            {
                MessageBox.Show(this, ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }
    }
}
