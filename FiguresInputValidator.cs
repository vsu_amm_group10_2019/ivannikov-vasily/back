﻿using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace Ivannikov
{
    public class FiguresInputValidator
    {
        public RichTextBox TxtWhiteFigures { get; }
        public RichTextBox TxtBlackFigures { get; }
        public TextBox TxtStartRookCoords { get; }
        public TextBox TxtStopRookCoords { get; }
        public RadioButton RadWhiteRookFigure { get; }
        public int BoardHeight { get; }
        public int BoardWidth { get; }

        public FiguresInputValidator(RichTextBox txtWhiteFigures, RichTextBox txtBlackFigures, TextBox txtStartRookCoords,
            TextBox txtStopRookCoords, RadioButton radWhiteRookFigure, int boardHeight,
            int boardWidth)
        {
            TxtWhiteFigures = txtWhiteFigures;
            TxtBlackFigures = txtBlackFigures;
            TxtStartRookCoords = txtStartRookCoords;
            TxtStopRookCoords = txtStopRookCoords;
            RadWhiteRookFigure = radWhiteRookFigure;
            BoardHeight = boardHeight;
            BoardWidth = boardWidth;
        }

        public Point ParseCoordsLine(string line)
        {
            int[] numbers = line.Split(' ')
                    .Where(x => int.TryParse(x, out var number))
                    .Select(x => int.Parse(x))
                    .ToArray();

            if (numbers.Length != 2)
                throw new ValidateException($"Строка \"{line}\" должна иметь два числа. ");

            int width = numbers[0];
            int height = numbers[1];

            if (width < 0 || height < 0 || width > BoardWidth || height > BoardHeight)
                throw new ValidateException($"Координаты в строке \"{line}\" должна быть в пределах размеров доски. ");

            return new Point(width, height);
        }

        private Point[] GetPoints(RichTextBox text)
            => text.Lines.Select(x => ParseCoordsLine(x)).ToArray();

        public int[,] GetBoard(int enemyFigureIndex, int friendFigureIndex)
        {
            Point startRookCoords = ParseCoordsLine(TxtStartRookCoords.Text);
            if (startRookCoords.X < 0 || startRookCoords.Y < 0 || startRookCoords.X > BoardWidth || startRookCoords.Y > BoardHeight)
                throw new ValidateException("Координаты ладьи находятся за пределами доски.");

            Point stopRookCoords = ParseCoordsLine(TxtStopRookCoords.Text);
            if (stopRookCoords.X < 0 || stopRookCoords.Y < 0 || stopRookCoords.X > BoardWidth || stopRookCoords.Y > BoardHeight)
                throw new ValidateException("Координаты точки назначения находятся за пределами доски.");

            var array = new int[BoardHeight, BoardWidth];
            Point[] blackPoints = GetPoints(TxtBlackFigures);
            Point[] whitePoints = GetPoints(TxtWhiteFigures);

            IEnumerable<Point> intersection = blackPoints.Intersect(whitePoints);
            if (intersection.Any())
                throw new ValidateException($"Координаты { intersection.First() } дублируются.");

            foreach (Point point in blackPoints)
                array[point.Y, point.X] = RadWhiteRookFigure.Checked
                    ? enemyFigureIndex
                    : friendFigureIndex;

            foreach (Point point in whitePoints)
                array[point.Y, point.X] = RadWhiteRookFigure.Checked
                    ? friendFigureIndex
                    : enemyFigureIndex;

            int startRookCell = array[startRookCoords.Y, startRookCoords.X];
            if (startRookCell != friendFigureIndex)
                throw new ValidateException("Координаты ладьи должны быть на фигуре ее цвета.");

            int stopRookCell = array[stopRookCoords.Y, stopRookCoords.X];
            if (stopRookCell == friendFigureIndex)
                throw new ValidateException("Координаты ладьи не должны быть на фигуре ее цвета.");

            return array;
        }
    }
}
