﻿
namespace Ivannikov
{
    partial class MainForm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblBlackFigures = new System.Windows.Forms.Label();
            this.txtBlackFigures = new System.Windows.Forms.RichTextBox();
            this.txtWhiteFigures = new System.Windows.Forms.RichTextBox();
            this.lblWhiteFigures = new System.Windows.Forms.Label();
            this.lblAnswers = new System.Windows.Forms.Label();
            this.txtAnswers = new System.Windows.Forms.RichTextBox();
            this.btnSolve = new System.Windows.Forms.Button();
            this.lblRookColor = new System.Windows.Forms.Label();
            this.radWhiteRookColor = new System.Windows.Forms.RadioButton();
            this.radBlackRookColor = new System.Windows.Forms.RadioButton();
            this.lblStartRookCoords = new System.Windows.Forms.Label();
            this.txtStartRookCoords = new System.Windows.Forms.TextBox();
            this.txtStopRookCoords = new System.Windows.Forms.TextBox();
            this.lblStopRookCoords = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblBlackFigures
            // 
            this.lblBlackFigures.AutoSize = true;
            this.lblBlackFigures.Location = new System.Drawing.Point(12, 9);
            this.lblBlackFigures.Name = "lblBlackFigures";
            this.lblBlackFigures.Size = new System.Drawing.Size(99, 15);
            this.lblBlackFigures.TabIndex = 0;
            this.lblBlackFigures.Text = "Черные фигуры:";
            // 
            // txtBlackFigures
            // 
            this.txtBlackFigures.Location = new System.Drawing.Point(12, 27);
            this.txtBlackFigures.Name = "txtBlackFigures";
            this.txtBlackFigures.Size = new System.Drawing.Size(213, 96);
            this.txtBlackFigures.TabIndex = 0;
            this.txtBlackFigures.Text = "";
            // 
            // txtWhiteFigures
            // 
            this.txtWhiteFigures.Location = new System.Drawing.Point(231, 27);
            this.txtWhiteFigures.Name = "txtWhiteFigures";
            this.txtWhiteFigures.Size = new System.Drawing.Size(213, 96);
            this.txtWhiteFigures.TabIndex = 1;
            this.txtWhiteFigures.Text = "";
            // 
            // lblWhiteFigures
            // 
            this.lblWhiteFigures.AutoSize = true;
            this.lblWhiteFigures.Location = new System.Drawing.Point(231, 9);
            this.lblWhiteFigures.Name = "lblWhiteFigures";
            this.lblWhiteFigures.Size = new System.Drawing.Size(91, 15);
            this.lblWhiteFigures.TabIndex = 0;
            this.lblWhiteFigures.Text = "Белые фигуры:";
            // 
            // lblAnswers
            // 
            this.lblAnswers.AutoSize = true;
            this.lblAnswers.Location = new System.Drawing.Point(12, 126);
            this.lblAnswers.Name = "lblAnswers";
            this.lblAnswers.Size = new System.Drawing.Size(41, 15);
            this.lblAnswers.TabIndex = 2;
            this.lblAnswers.Text = "Ответ:";
            // 
            // txtAnswers
            // 
            this.txtAnswers.BackColor = System.Drawing.SystemColors.Window;
            this.txtAnswers.Location = new System.Drawing.Point(12, 144);
            this.txtAnswers.Name = "txtAnswers";
            this.txtAnswers.ReadOnly = true;
            this.txtAnswers.Size = new System.Drawing.Size(432, 96);
            this.txtAnswers.TabIndex = 3;
            this.txtAnswers.TabStop = false;
            this.txtAnswers.Text = "";
            // 
            // btnSolve
            // 
            this.btnSolve.Location = new System.Drawing.Point(10, 272);
            this.btnSolve.Name = "btnSolve";
            this.btnSolve.Size = new System.Drawing.Size(432, 23);
            this.btnSolve.TabIndex = 3;
            this.btnSolve.Text = "Решить";
            this.btnSolve.UseVisualStyleBackColor = true;
            this.btnSolve.Click += new System.EventHandler(this.BtnSolvedClicked);
            // 
            // lblRookColor
            // 
            this.lblRookColor.AutoSize = true;
            this.lblRookColor.Location = new System.Drawing.Point(12, 247);
            this.lblRookColor.Name = "lblRookColor";
            this.lblRookColor.Size = new System.Drawing.Size(71, 15);
            this.lblRookColor.TabIndex = 4;
            this.lblRookColor.Text = "Цвет ладьи:";
            // 
            // radWhiteRookColor
            // 
            this.radWhiteRookColor.AutoSize = true;
            this.radWhiteRookColor.Checked = true;
            this.radWhiteRookColor.Location = new System.Drawing.Point(90, 247);
            this.radWhiteRookColor.Name = "radWhiteRookColor";
            this.radWhiteRookColor.Size = new System.Drawing.Size(61, 19);
            this.radWhiteRookColor.TabIndex = 2;
            this.radWhiteRookColor.TabStop = true;
            this.radWhiteRookColor.Text = "Белый";
            this.radWhiteRookColor.UseVisualStyleBackColor = true;
            // 
            // radBlackRookColor
            // 
            this.radBlackRookColor.AutoSize = true;
            this.radBlackRookColor.Location = new System.Drawing.Point(157, 247);
            this.radBlackRookColor.Name = "radBlackRookColor";
            this.radBlackRookColor.Size = new System.Drawing.Size(69, 19);
            this.radBlackRookColor.TabIndex = 3;
            this.radBlackRookColor.TabStop = true;
            this.radBlackRookColor.Text = "Чёрный";
            this.radBlackRookColor.UseVisualStyleBackColor = true;
            // 
            // lblStartRookCoords
            // 
            this.lblStartRookCoords.AutoSize = true;
            this.lblStartRookCoords.Location = new System.Drawing.Point(232, 247);
            this.lblStartRookCoords.Name = "lblStartRookCoords";
            this.lblStartRookCoords.Size = new System.Drawing.Size(41, 15);
            this.lblStartRookCoords.TabIndex = 5;
            this.lblStartRookCoords.Text = "Старт:";
            // 
            // txtStartRookCoords
            // 
            this.txtStartRookCoords.Location = new System.Drawing.Point(279, 244);
            this.txtStartRookCoords.Name = "txtStartRookCoords";
            this.txtStartRookCoords.Size = new System.Drawing.Size(43, 23);
            this.txtStartRookCoords.TabIndex = 6;
            // 
            // txtStopRookCoords
            // 
            this.txtStopRookCoords.Location = new System.Drawing.Point(399, 244);
            this.txtStopRookCoords.Name = "txtStopRookCoords";
            this.txtStopRookCoords.Size = new System.Drawing.Size(43, 23);
            this.txtStopRookCoords.TabIndex = 8;
            // 
            // lblStopRookCoords
            // 
            this.lblStopRookCoords.AutoSize = true;
            this.lblStopRookCoords.Location = new System.Drawing.Point(342, 247);
            this.lblStopRookCoords.Name = "lblStopRookCoords";
            this.lblStopRookCoords.Size = new System.Drawing.Size(51, 15);
            this.lblStopRookCoords.TabIndex = 7;
            this.lblStopRookCoords.Text = "Финиш:";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(454, 307);
            this.Controls.Add(this.txtStopRookCoords);
            this.Controls.Add(this.lblStopRookCoords);
            this.Controls.Add(this.txtStartRookCoords);
            this.Controls.Add(this.lblStartRookCoords);
            this.Controls.Add(this.radBlackRookColor);
            this.Controls.Add(this.radWhiteRookColor);
            this.Controls.Add(this.lblRookColor);
            this.Controls.Add(this.btnSolve);
            this.Controls.Add(this.txtAnswers);
            this.Controls.Add(this.lblAnswers);
            this.Controls.Add(this.txtWhiteFigures);
            this.Controls.Add(this.txtBlackFigures);
            this.Controls.Add(this.lblWhiteFigures);
            this.Controls.Add(this.lblBlackFigures);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MainForm";
            this.Text = " Backtracking";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblBlackFigures;
        private System.Windows.Forms.RichTextBox txtBlackFigures;
        private System.Windows.Forms.RichTextBox txtWhiteFigures;
        private System.Windows.Forms.Label lblWhiteFigures;
        private System.Windows.Forms.Label lblAnswers;
        private System.Windows.Forms.RichTextBox txtAnswers;
        private System.Windows.Forms.Button btnSolve;
        private System.Windows.Forms.Label lblRookColor;
        private System.Windows.Forms.RadioButton radWhiteRookColor;
        private System.Windows.Forms.RadioButton radBlackRookColor;
        private System.Windows.Forms.Label lblStartRookCoords;
        private System.Windows.Forms.TextBox txtStartRookCoords;
        private System.Windows.Forms.TextBox txtStopRookCoords;
        private System.Windows.Forms.Label lblStopRookCoords;
    }
}

